package com.reksa.karang.simplequiz;

import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.reksa.karang.simplequiz.Model.TrueFalse;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    Button trueButton, falseButton;
    TextView textQuestion, textScore;
    int index, question, score;
    ProgressBar progressBar;

    private TrueFalse[] questionBank = new TrueFalse[] {
            new TrueFalse(R.string.question_1, true),
            new TrueFalse(R.string.question_2, true),
            new TrueFalse(R.string.question_3, true),
            new TrueFalse(R.string.question_4, true),
            new TrueFalse(R.string.question_5, true),
            new TrueFalse(R.string.question_6, false),
            new TrueFalse(R.string.question_7, true),
            new TrueFalse(R.string.question_8, false),
            new TrueFalse(R.string.question_9, true),
            new TrueFalse(R.string.question_10, true),
            new TrueFalse(R.string.question_11, false),
            new TrueFalse(R.string.question_12, false),
            new TrueFalse(R.string.question_13,true)
    };

    final int PROGRESS_BAR_INCREMENT = (int) Math.ceil(100.0 / questionBank.length);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            score = savedInstanceState.getInt("ScoreKey");
            index = savedInstanceState.getInt("IndexKey");
        } else {
            score = 0;
            index = 0;
        }

        trueButton = findViewById(R.id.true_button);
        falseButton = findViewById(R.id.false_button);
        textQuestion = findViewById(R.id.question_text_view);
        textScore = findViewById(R.id.score);
        progressBar = findViewById(R.id.progress_bar);

        question = questionBank[index].getQuestionID();
        textQuestion.setText(question);

        textScore.setText("You scored " + score + "/" + questionBank.length);

        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
                updateQuestion();
            }
        });

        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
                updateQuestion();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putInt("ScoreKey", score);
        outState.putInt("IndexKey", index);
    }

    private void updateQuestion() {
        index = (index + 1) % questionBank.length;

        if (index == 0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Game Over");
            alert.setCancelable(false);
            alert.setMessage("You scored " + score + " Points!");
            alert.setPositiveButton("Close Application ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).show();
        }

        question = questionBank[index].getQuestionID();
        textQuestion.setText(question);
        progressBar.incrementProgressBy(PROGRESS_BAR_INCREMENT);
        textScore.setText("Score " + score + "/" + questionBank.length);

    }

    private void checkAnswer(boolean userSelection) {
        boolean correctAnswer = questionBank[index].isAnswer();

        if (userSelection == correctAnswer) {
            Toast.makeText(this, R.string.correct_toast, Toast.LENGTH_SHORT).show();
            score++;
        } else {
            Toast.makeText(this, R.string.incorrect_toast, Toast.LENGTH_SHORT).show();
        }
    }
}
































